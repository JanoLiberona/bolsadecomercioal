<%-- 
    Document   : index
    Created on : 29-04-2021, 16:03:31
    Author     : ccruces
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CCBolsa | Home</title>
  <style>
*{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
body{margin:0px; padding:0px; font-family: 'Open Sans', sans-serif;}
section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
h1{margin:0px auto; display:table; font-size:26px; padding:40px 0px; color:#002e5b; text-align:center;}
h1 span{font-weight:500;}

header{width:100%; display:table; background-color:#fde428; margin-bottom:50px;}
#logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
nav{width:auto; float:right;}
nav ul{display:table; float:right;}
nav ul li{float:left;}
nav ul li:last-child{padding-right:0px;}
nav ul li a{color:#002e5b; font-size:18px; padding: 25px 20px; display:inline-block; transition: all 0.5s ease 0s;}
nav ul li a:hover{background-color:#002e5b; color:#fde428; transition: all 0.5s ease 0s;}
nav ul li a:hover i{color:#fde428; transition: all 0.5s ease 0s;}
nav ul li a i{padding-right:10px; color:#002e5b; transition: all 0.5s ease 0s;}

.toggle-menu ul{display:table; width:25px;}
.toggle-menu ul li{width:100%; height:3px; background-color:#002e5b; margin-bottom:4px;}
.toggle-menu ul li:last-child{margin-bottom:0px;}

input[type=checkbox], label{display:none;}

.content{display:table; margin-bottom:60px; width:900px;}
.content h2{font-size:18px; font-weight:500; color:#002e5b; border-bottom:1px solid #fde428; display:table; padding-bottom:10px; margin-bottom:10px;}
.content p{font-size:14px; line-height:22px; color:#7c7c7c; text-align:justify;}

footer{display:table; padding-bottom:30px; width:100%;}
.social{margin:0px auto; display:table; display:table;}
.social li{float:left; padding:0px 10px;}
.social li a{color:#002e5b; transition: all 0.5s ease 0s;}
.social li a:hover{color:#fde428; transition: all 0.5s ease 0s;}

@media only screen and (max-width: 1440px) {
section{max-width:95%;}
}

@media only screen and (max-width: 980px) {
header{padding:20px 0px;}
#logo{padding:0px;}
input[type=checkbox] {position: absolute; top: -9999px; left: -9999px; background:none;}
input[type=checkbox]:fous{background:none;}
label {float:right; padding:8px 0px; display:inline-block; cursor:pointer; }
input[type=checkbox]:checked ~ nav {display:block;}

nav{display:none; position:absolute; right:0px; top:53px; background-color:#002e5b; padding:0px; z-index:99;}
nav ul{width:auto;}
nav ul li{float:none; padding:0px; width:100%; display:table;}
nav ul li a{color:#FFF; font-size:15px; padding:10px 20px; display:block; border-bottom: 1px solid rgba(225,225,225,0.1);}
nav ul li a i{color:#fde428; padding-right:13px;}
}

@media only screen and (max-width: 980px) {
.content{width:90%;}
}

@media only screen and (max-width: 568px) {
h1{padding:25px 0px;}
h1 span{display:block;}
}

@media only screen and (max-width: 480px) {
section {max-width: 90%;}
}

@media only screen and (max-width: 360px) {
h1{font-size:20px;}
label{padding:5px 0px;}
#logo{font-size: 20px;}
nav{top:47px;}
}

@media only screen and (max-width: 320px) {
h1 {padding: 20px 0px;}
}

</style>
</head>
    <body>
        <h1><span>Bolsa Comercio de Cii-Usted no esta autorizado para ver esta pagina</span> </h1>

<header>
<section>
<a href="yur" id="logo" target="_blank"></a>

<label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>
<input type="checkbox" id="toggle-1">

<nav>
<ul>
<li><a href="LoginController"><i class="icon-home"></i>Login</a></li>    
<li><a href="RegistrarController"><i class="icon-home"></i>Registrar</a></li>
<li><a href="RegistrarController"><i class="icon-home"></i>Backoffice</a></li>
</ul>
</nav>
</header>

</section>

<section id="about" class="content">
<h2>About</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</section>

<section id="portfolio" class="content">
<h2>Portfolio</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</section>

<section id="services" class="content">
<h2>Services</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</section>

<section id="gallery" class="content">
<h2>Gallery</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</section>

<section id="contact" class="content">
<h2>Contact</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</section>

<footer>
<ul class="social">
<li><a href="https://twitter.com/mayursuthar2693" target="_blank"><i class="icon-twitter"></i></a></li>
<li><a href="https://www.facebook.com/mayursuthar2693" target="_blank"><i class="icon-facebook"></i></a></li>
<li><a href="https://www.linkedin.com/in/sutharmayur" target="_blank"><i class="icon-linkedin"></i></a></li>
<li><a href="https://www.pinterest.com/MayurSuthar2693/" target="_blank"><i class="icon-pinterest"></i></a></li>
<li><a href="https://plus.google.com/109916819421919014146/posts" target="_blank"><i class="icon-google-plus"></i></a></li>
<li><a href="https://www.instagram.com/mayursuthar2693/" target="_blank"><i class="icon-instagram"></i></a></li>
</ul>
</footer>
        
        
    </body>
 </html>
