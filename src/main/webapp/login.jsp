<%-- 
    Document   : index
    Created on : 22-04-2021, 19:31:00
    Author     : Jano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>JSP Page</title>
    </head>
    
    
    <h2>Login Page</h2><br>
    <div class="login">
        
        
        
        <form name="form" action="LoginController" method="POST">
            <label><b>User Name
            </b>
            </label>
            <input type="text" name="Uname" id="Uname" placeholder="Username">
            <br><br>
            <label><b>Password
            </b>
            </label>
            <input type="Password" name="Pass" id="Pass" placeholder="Password">
            <br><br>
            <input type="submit" name="log" value="Log in Here">
            <br><br>
            <input type="checkbox" id="check">
            <span>Remember me</span>
            <br><br>
            Forgot <a href="#">Password</a>
        </form>
    </div>
</html>
