/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercio.dto;

/**
 *
 * @author Jano
 */
public class AccionDto {
    
private String nombreAccion;
  private String precioCompra;
  private String precioUltimo;
  private String precioVenta;


 // Getter Methods 

  public String getNombreAccion() {
    return nombreAccion;
  }

  public String getPrecioCompra() {
    return precioCompra;
  }

  public String getPrecioUltimo() {
    return precioUltimo;
  }

  public String getPrecioVenta() {
    return precioVenta;
  }

 // Setter Methods 

  public void setNombreAccion( String nombreAccion ) {
    this.nombreAccion = nombreAccion;
  }

  public void setPrecioCompra( String precioCompra ) {
    this.precioCompra = precioCompra;
  }

  public void setPrecioUltimo( String precioUltimo ) {
    this.precioUltimo = precioUltimo;
  }

  public void setPrecioVenta( String precioVenta ) {
    this.precioVenta = precioVenta;
  }
}

