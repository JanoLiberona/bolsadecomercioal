/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercioal.DAO;

import cl.bolsadecomercioal.DAO.exceptions.NonexistentEntityException;
import cl.bolsadecomercioal.DAO.exceptions.PreexistingEntityException;
import cl.bolsadecomercioal.entity.Acciones;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jano
 */
public class AccionesJpaController implements Serializable {

    public AccionesJpaController() {
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_BOLSA_DE_COMERCIO");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Acciones acciones) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(acciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAcciones(acciones.getNombre()) != null) {
                throw new PreexistingEntityException("Acciones " + acciones + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Acciones acciones) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            acciones = em.merge(acciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = acciones.getNombre();
                if (findAcciones(id) == null) {
                    throw new NonexistentEntityException("The acciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Acciones acciones;
            try {
                acciones = em.getReference(Acciones.class, id);
                acciones.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The acciones with id " + id + " no longer exists.", enfe);
            }
            em.remove(acciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Acciones> findAccionesEntities() {
        return findAccionesEntities(true, -1, -1);
    }

    public List<Acciones> findAccionesEntities(int maxResults, int firstResult) {
        return findAccionesEntities(false, maxResults, firstResult);
    }

    private List<Acciones> findAccionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Acciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Acciones findAcciones(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Acciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getAccionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Acciones> rt = cq.from(Acciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
