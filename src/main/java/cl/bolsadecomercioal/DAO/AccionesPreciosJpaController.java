/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercioal.DAO;

import cl.bolsadecomercioal.DAO.exceptions.NonexistentEntityException;
import cl.bolsadecomercioal.DAO.exceptions.PreexistingEntityException;
import cl.bolsadecomercioal.entity.AccionesPrecios;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jano
 */
public class AccionesPreciosJpaController implements Serializable {

    public AccionesPreciosJpaController() {
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_BOLSA_DE_COMERCIO");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AccionesPrecios accionesPrecios) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(accionesPrecios);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAccionesPrecios(accionesPrecios.getNombre()) != null) {
                throw new PreexistingEntityException("AccionesPrecios " + accionesPrecios + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AccionesPrecios accionesPrecios) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            accionesPrecios = em.merge(accionesPrecios);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = accionesPrecios.getNombre();
                if (findAccionesPrecios(id) == null) {
                    throw new NonexistentEntityException("The accionesPrecios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AccionesPrecios accionesPrecios;
            try {
                accionesPrecios = em.getReference(AccionesPrecios.class, id);
                accionesPrecios.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The accionesPrecios with id " + id + " no longer exists.", enfe);
            }
            em.remove(accionesPrecios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AccionesPrecios> findAccionesPreciosEntities() {
        return findAccionesPreciosEntities(true, -1, -1);
    }

    public List<AccionesPrecios> findAccionesPreciosEntities(int maxResults, int firstResult) {
        return findAccionesPreciosEntities(false, maxResults, firstResult);
    }

    private List<AccionesPrecios> findAccionesPreciosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AccionesPrecios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AccionesPrecios findAccionesPrecios(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AccionesPrecios.class, id);
        } finally {
            em.close();
        }
    }

    public int getAccionesPreciosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AccionesPrecios> rt = cq.from(AccionesPrecios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
