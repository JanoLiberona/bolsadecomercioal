/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercioal.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jano
 */
@Entity
@Table(name = "acciones_precios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccionesPrecios.findAll", query = "SELECT a FROM AccionesPrecios a"),
    @NamedQuery(name = "AccionesPrecios.findByNombre", query = "SELECT a FROM AccionesPrecios a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "AccionesPrecios.findByPrecioventa", query = "SELECT a FROM AccionesPrecios a WHERE a.precioventa = :precioventa"),
    @NamedQuery(name = "AccionesPrecios.findByPreciocompra", query = "SELECT a FROM AccionesPrecios a WHERE a.preciocompra = :preciocompra"),
    @NamedQuery(name = "AccionesPrecios.findByPrecioultimatransaccion", query = "SELECT a FROM AccionesPrecios a WHERE a.precioultimatransaccion = :precioultimatransaccion")})
public class AccionesPrecios implements Serializable {

    @Size(max = 2147483647)
    @Column(name = "precioventa")
    private String precioventa;
    @Size(max = 2147483647)
    @Column(name = "preciocompra")
    private String preciocompra;
    @Size(max = 2147483647)
    @Column(name = "precioultimatransaccion")
    private String precioultimatransaccion;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;

    public AccionesPrecios() {
    }

    public AccionesPrecios(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccionesPrecios)) {
            return false;
        }
        AccionesPrecios other = (AccionesPrecios) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.bolsadecomercioal.entity.AccionesPrecios[ nombre=" + nombre + " ]";
    }

    public String getPrecioventa() {
        return precioventa;
    }

    public void setPrecioventa(String precioventa) {
        this.precioventa = precioventa;
    }

    public String getPreciocompra() {
        return preciocompra;
    }

    public void setPreciocompra(String preciocompra) {
        this.preciocompra = preciocompra;
    }

    public String getPrecioultimatransaccion() {
        return precioultimatransaccion;
    }

    public void setPrecioultimatransaccion(String precioultimatransaccion) {
        this.precioultimatransaccion = precioultimatransaccion;
    }
    
}
