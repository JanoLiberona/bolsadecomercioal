/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercioal.controller;

import cl.bolsadecomercioal.DAO.AccionesJpaController;
import cl.bolsadecomercioal.DAO.AccionesPreciosJpaController;
import cl.bolsadecomercioal.DAO.exceptions.NonexistentEntityException;
import cl.bolsadecomercioal.entity.Acciones;
import cl.bolsadecomercioal.entity.AccionesPrecios;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jano
 */
@WebServlet(name = "MantenerAcciones", urlPatterns = {"/MantenerAcciones"})
public class MantenerAcciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantenerAcciones</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantenerAcciones at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AccionesJpaController dao = new AccionesJpaController();
        List<Acciones> acciones = dao.findAccionesEntities();

        request.setAttribute("listaAcciones", acciones);

        request.getRequestDispatcher("mantenedor_acciones.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("accion");

        
        if(boton.equals("creargrabar")){
            
            Acciones accion = new Acciones();
            accion.setNombre(request.getParameter("nombre"));
            accion.setDescripcion(request.getParameter("descripcion"));
            AccionesJpaController dao = new AccionesJpaController();
            try {
                dao.create(accion);
            } catch (Exception ex) {
                Logger.getLogger(MantenerAcciones.class.getName()).log(Level.SEVERE, null, ex);
            }
            List<Acciones> acciones = dao.findAccionesEntities();

            request.setAttribute("listaAcciones", acciones);
            request.getRequestDispatcher("mantenedor_acciones.jsp").forward(request, response);
        }
        
        if (boton.equals("agregar")) {
            request.getRequestDispatcher("crear_accion.jsp").forward(request, response);
        }
        if (boton.equals("eliminar")) {
            String id = request.getParameter("seleccion");
            AccionesJpaController dao = new AccionesJpaController();
            try {
                dao.destroy(id);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(MantenerAcciones.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<Acciones> acciones = dao.findAccionesEntities();

            request.setAttribute("listaAcciones", acciones);
            request.getRequestDispatcher("mantenedor_acciones.jsp").forward(request, response);

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
