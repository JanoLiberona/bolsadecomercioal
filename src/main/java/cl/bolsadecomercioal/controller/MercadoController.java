/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bolsadecomercioal.controller;

import cl.bolsadecomercio.dto.AccionDto;
import cl.bolsadecomercioal.DAO.AccionesPreciosJpaController;
import cl.bolsadecomercioal.entity.AccionesPrecios;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jano
 */
@WebServlet(name = "MercadoController", urlPatterns = {"/MercadoController"})
public class MercadoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MercadoController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MercadoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        /*   AccionesPreciosJpaController dao = new AccionesPreciosJpaController();
            List<AccionesPrecios> listaAcciones=dao.findAccionesPreciosEntities();
            request.setAttribute("listaAcciones", listaAcciones);*/
        List<AccionDto> listaAcciones = new ArrayList<AccionDto>();
        Client client = ClientBuilder.newClient();
        WebTarget recurso = client.target("http://localhost:9090/apibolsadecomercioal/api/accion");
        listaAcciones = recurso.request(MediaType.APPLICATION_JSON).get(new GenericType<List<AccionDto>>() {
        });

        request.setAttribute("listaAcciones", listaAcciones);
        request.getRequestDispatcher("detalle_mercado.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
